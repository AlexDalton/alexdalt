#!/bin/sh

convert "$1" -sampling-factor 4:2:0 -strip -quality 70 -resize 2000x2000 -interlace JPEG -colorspace sRGB "compressed/${1%%.*}.jpg"
