---
title: "The Briarhack"
draft: false
archive: false
description: "A draft of a rules light tabletop roleplaying game featuring character development as an emergent property of play."
thumb: "thumb.jpg"
collections: ['Rules']
date: 2024-06-03
external: "https://gitlab.com/gringolet-games/briarlock/-/releases"
---
