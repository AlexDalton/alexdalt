---
title: "Missives In The Mire"
draft: false
archive: false
description: "A simultaneous-order-alternating-activations rules hack for Warhammer: The Old World"
date: 2024-06-05
collections: ['Rules']
thumb: "thumb.jpg"
---

**Missives in the Mire** is a rules hack for Warhammer: The Old World which
aims to simulate the chaotic tumult of battle, with orders arriving from a
central commander and officers entrusted with their effective execution. This hack
replaces the traditional you-go-I-go four phase turn structure as given in
Warhammer: The Old World with
with hidden simultaneous orders and alternating activations:

1. **Strategy Phase**, largely the same as the original rules but without
spellcasting,
2. **Issuing Orders** in which both generals simultaneously issue hidden orders to each unit under their
control,
3. and **Executing Orders** in which each general takes it in turns to active a single
unit, executing their orders

Orders that made sense to issue at the beginning of the round may quickly become
ineffective or suicidal, and generals must carefully assign orders as to allow
their units to be as effective as possible when **Executing Orders**. This hack adds
a dimension of deduction to the game, as players attempt to guess and bluff
orders, as well as an extra layer of decision making when prioritising the order
of unit activations.

I have been told this hack shares a design philosophy with Legions Imperialis,
although I have yet to have the pleasure of reading or playing that
particular ruleset. My own inspiration has bubbled up from many evenings playing
The Game Of Thrones Board Game. An imperfect game, but one that captures the
feeling of mismanaged armies and moment-to-moment tactical exploitation well.

Unless otherwise stated all rules as presented in the Warhammer: The Old World 
apply.
If this hack introduces a rules conflict that I have not considered try to agree
on a ruling based on what would be most interesting for both players. Otherwise
flip a coin.

# Setting Up

Players set up as usual.

# The Turn Structure

## Strategy Phase

Both players resolve the Strategy Phase. In the case of any interactions (due to
spell casting or otherwise) whoever finished activating their units in the
previous round may perform a single action, and then their opponent may perform a
single action. **No spells are cast during this phase**.

## Issuing Orders

For all units under their control, both
generals simultaneously issue one of the following orders in secret:

- **Manoeuvre**, allowing units to move as normal, shoot if able, and cast spells if able.
- **March**, allowing units to move double distance, forfeiting shooting and casting spells.
- **Charge**, allowing units to charge into combat, and resolve a round of combat.
- **Fight**, this order can only be issued to units currently engaged in combat,
is the only order that can be issued to units currently engaged in combat,
and allows this unit and any engaged to resolve a round of combat.

I recommend using marked poker chips.

Once all orders are issued, they are all revealed.

## Executing Orders

Each general takes turns activating a unit with unresolved orders.
On the first turn, whoever finished setting up their army first goes first, and on subsequent
turns, whoever finished activating their units first in the previous turn goes first.

### Manoeuvre

A unit with this order:

1. Optionally, performs a standard move action (as described in the original Movement phase)
2. Optionally, performs a ranged attack if able (as descried in the original Shooting phase)
3. Optionally, casts any and all spells available

### March

A unit with this order moves double distance, as usual for a **March** move in the
original Movement phase.

### Charge

A unit with this order:

1. May cast any spells that would usually be available to this unit.
2. Perform a charge action.
3. Resolve a combat, or exchange order for **Fight**, or nothing.

If this unit becomes engaged with the enemy a combat is resolved
between all participants in this particular fight, as described in the Combat
Phase in the original rules. All friendly units in the combat remove their orders
and count as having activated.

However, if charging into a fight with existing engaged friendly units that do
not have yet-unresolved orders, i.e. they have already triggered a round of
combat, no combat takes place.

Optionally, the charging unit can forgo triggering a round of combat and switches
their order to a **Fight** order. They may wish to do this to defer the combat
until more friendly charging participants have joined the fray. They may well still
count as charging in the subsequent combat.

A unit only counts as charging in a combat if they resolved a **Charge** action 
this turn and the current combat being resolved was triggered as a result of
resolving an order on a friendly unit.

## Fight

A unit with this order:

1. Cast any spells that would usually be available to this unit, unless they charged this round.
2. Resolve a single combat as described in the Combat phase of the original rules.
3. Remove any orders from all friendly units
engaged in the fight.

With the restrictions on combat triggered by **Charge**ing,
no unit should be able to fight more than twice in a single round.
