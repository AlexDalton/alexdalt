---
title: "DiSCuS"
draft: false
archive: false
description: "A decentralised distributed computation framework constructed with arithmetic circuits"
thumb: "thumb.png"
date: 2022-03-01
external: "https://gitlab.com/AlexDaltonUni/PhD/discus"
---
