---
title: "Thanks for Coming"
draft: false
archive: false
description: "An extended play and short film by The Pylons"
thumb: "thumb.jpg"
date: 2022-11-02
---

{{< youtube src="https://www.youtube.com/embed/FvRuTBrSlM4" >}}

{{< spotify src="https://open.spotify.com/embed/album/0TMsFeLLEhUVoMgVSvzC10?utm_source=generator&theme=0" height="300px">}}

{{< load-photoswipe >}}
{{< gallery dir="thanks_for_coming/" caption-position="none" />}}
