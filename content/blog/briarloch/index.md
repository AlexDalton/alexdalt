---
title: "Briarloch"
date: 2022-06-27T19:25:45+01:00
categories: ["ttrpg"]
aliases:
    - "/bf96cf4a"
draft: true
description: "Hello"
thumb: 'thumb.jpg'
---

The Shaded City, a strange twilight land-locked city state in the heart of an
otherwise well controlled
empire.
Controlled by The Guild of Lochmen, a not-so-secret society, and surrounded by
gargantuan impenetrable walls,
Briarloch is a rich city which has managed to stay independent through a subtle
stranglehold on overland trade and a untameable wild forest which claims the
lives of any who stray from the vigilantly maintained official roads. Many have
tried to claim Briarloch, but none have first tamed The Briar.

Equal parts Gormenghast and New York, the city is a knotted mess of crooked alley ways and vertically clambering
hunched buildings. The small footprint of the city has lead to a labyrinthine
overlapping haphazard style of construction, where the buildings themselves
appear as light-starved plants climbing from an industrial forest floor. The
city does not have districts in a traditional sense and instead favours levels.

Ancient, with a history lost to time. The mysteries of the city are countless,
not least being; what came first, the city or it's encircling wode.

# Locations of Interest

## The Great Roads

The upper most layer of Briarloch is the most clearly defined. Four huge roads,
one in each cardinal direction, meet at a central cross. Suspended above the
core mass of the city by gargantuan pillars as if an aqueduct, the climbing
buildings of the layers below reach for these structures.

The roads are busy, always. Most traffic simply passing through the city as
quickly as possible. This is the only area of the city navigable by anyone
non-local.

The most expensive real estate and locations for the most lucrative trade are
located along the edge of this road. With more dubious establishments just off
the main thoroughfare (with less
safety-guaranteeing prices) placed tactically close to the road to catch the eye
of an unweary penny-pinching traveller.

* Heroes Spectacle; the great fighting pit. A huge area of the city completely
empty apart from regular monoliths defining the outer perimiter. On fight days
the arena rises out of the ground in thirds.
* The Demon's Cellar; a hipster underground tavern which is more show than grit
* Moza's Maske Shop; a goblin-run mask shop, traditional, masquerade.
* The Swampy Pot Antique Shop; run by Ramili, specialises in trinkets and
treasure dug up in The North.
* Sun Gull Tailors; Kotilla, a Aracrokra woman, deals in the most incredible
finery
* Alabaster Abundance; gem cutter Nola (halfling)
* The Brass Flask; a converted manor house into a tavern and inn
* Dapper Gentleman's Arsenal and Armorer; animated wooden man motions into the
shop. Run by Ariana Twoorb, a half elven woman. Specialises in fine parade mail
can make Armor of Gleaming to order.
* The Repository; a massive subterranean archive full of scrolls and historical
notes, manned by The Triumphant Concordance
* The Ruby Cask; a dingy pub with a great ruby cask taking up a whole wall. You
pay for a mug and drink until you're done.
* Col's Fish Shoppe, a weather beaten shack selling smelly fish. Run by a
weather beaten man who remembers everyone who's ever shopped here

## The Delve

As we descends deeper and deeper into the earth, hopefully guided by a well-paid
and resourceful local, we enter a
great sinkhole carved into the depths. After the collapse of the dwarven
kingdoms and the ancient and great emigration, many dwarves found a second home
in the industrial lower levels of Briarloch.

* Earthen Scriptorium; a collection of scrolls and books dug deep into the
ground, the ancient dwarf (Old Gorgrin) who tends the collection doesn't even
know how deep the tunnels go!
* The Minstrel's Cap; a taproom made of timber built on the edge of The Delve,
the back portion of the tavern fell into the pit, taking the stage with it
* Gorehame's Exotic Animals; Gorehame is a retired dwarven hunter with his dog
Steven. Almiraj, Bood Hawk, Mastiff.
* Zaghain's Hauberk's; deep in an cramped alleyway a huge internal
workshop/shop female dward named Zaghain runs the shop, can provide regular
armour in dwarven design
* Across from Zaghain's is Sibauss' Blades, run my an eleven man Sibauss.
Provide finely crafted elven weapons
* An entrance to The Goblin Nightmarket made visible to those worthy of it's
coin, a winding alley way doubles back on itself, folds in impossible corners
and then ends a tiny little wooden door with the symbol on it. This exits in a
tree stump in a corner of Arcadia where comerce is king
* The Gnome And Half-wit; a rough fighting pit run by an unhinged aging gnome
who took down the old owner (a half-ogre) and won the deed.

## The Guild Way

And bellow all that exists The Guild Way. Where Briarloch is an organic maze of
structures and alleys, The Guild Way is a cryptic labyrinth, intentionally
engineered to waylay all but the most trained mind. Any who find themselves in
The Guild Way become lost unless under exceptional circumstances.

They become lost for d8 hours until eventually emerging in. This path is not
repeatable and even magic does not make The Guild Way more navigable.

| Doorway                                   | Location  |
| ------------------                        | --------  |
| Behind a fireplace                        | Tavern    |
| Behind a bookcase                         | Shop      |
| A hidden door in an otherwise smooth wall | Residence |
| Trap door or sewer grate                  | Alleyway  |

No matter where you exit you will be a surprise to whoever awaits you there.
Only members of the guild know these entrances and exists and only in situations
where they will be of use.

# Organisations

## The Guild of Lochmen

As far as the public can see, Guild representatives are all seemingly identical clones of
indeterminate gender. Each of which looks like an average of all faces across
the most common sentient races. They handle every aspect of city life; they are
civil servants, law enforcement, and even are employed to dispose of waste.

They are even tempered to a fault. Have an
affinity for paperwork, and can do tithe calculus in their heads. They always
appear in threes. To a very astute observer they have minute differences between
them. More subtle than the differences between identical twins, hairs-width
differences in height, imperceptible variations in speech patterns and
vocabulary.

### Their Secret

The Guild members as they are known to the public, as homogeneous clones are all
assimilated criminals, political dissidents, and economic competitors to the
monopolies of The Guild. They were disappeared and taken deep into The Guild
Way, to the factory-prison, The Blackwater. Here they have their minds wiped
through prolonged exposure to hyper-intelligent psychic oozes, have their faces
wiped through proto-alchemical surgery, and are then reprogrammed via
meta-hypnosis. However the conditioning is imperfect and on rare occasions
cracks form in their new loyal minds. This is why they always appear in threes.

They are also only the public facing wing of The Guild. Beyond eyeshot is a
intricate and ancient bureaucratic topology of vampires. Infinite offices and
tithe halls of vampires in forced subservience to their superior, the one who
sired them.

At the very top of this hierarchy is (look up name and title I came up with), he
has sat atop the alabaster guild-throne for so long he has calcified to it.
Partially formed into undead-vampire-fossil. Driven equal parts mad and
prophetic from the pain and knowledge of the stone. Stone which comprises the
ancient city of Briarloch. Stones which form the deepest foundation of the city.
Stones that hear and feel the totality of the life above as if the inner
workings of ones own body.

## The League of The Iron Key

### Their Secret

## The Empire (or equivalent for your setting)

### Their Secret

## Quest Lines & Grim Portents

    The Celebrants of Rebirth Cultists [Aurora Borealis/Disappearances/The Moon Looks… Thin…]
    Festival of the Five Fates [Day of Pride/Day of Luck/Day of Piety/Day of Furtiveness/Day of Honour]
    The Goblin Night Market [Goblin Coin In Pocket/Nursery Rhyme With Location Clues/The Night of The Night Market]
    The Guild of Lochmen & The League of the Iron Key [Scouted/Imprisoned/Escape]
    The Oni [Another sighting of the old man/toying with them/hunting them]

# Festival of the Five Fates

Multiple motivations, League of the Iron Key want to destabalise the guild, the
guild want to quash the league, the empire want to sow dissent.

## Approaching Oberloch

## Small Bouts at The Gnome and Halfwit

    Team fight gains entry into the tournament of the first fate
    Single combat for a cash prize and betting
	

## The Tournament of the 1st Fate
    Part of the festival of the five fates (honour, pride, luck, piety, furtiveness)
    Five days of celebration ending in The Tournament of the 1st Fate
        Unordered List Item
        Drinking
        Feasting
        Gift Giving (wooden swords, crowns, dice, rings, and masks; all painted and decorated by the gift giver)
    Chariot Races
    Griffon Jousting
    Ogre slaying
    Public Execution
    Players act out the Battle of the Byway (less serious criminals can fight for a chance to earn a reprive as the losing side)
        First the elite troops of Oberloch fought there way through monsters
            Then a legion of Ascanan Royal Guard
            Then fell foul of a mind control curse the sneaky Ascanans had placed on them, probably with a DEVIL PACT
            Only a lone hero returned, securing the future of Oberlock and saving it's people from Ascanan rule!
    The guild have commissioned a special magic item for the winner
	
## The Blackwater

The players enter the tense political game of Oberloch. They spend some time in the city and experience it's eccentricities, The Guild of Lochmen hold an invisible hand around the throat of the city and the alliance with the Ascanan Protectorate grows tenser with every passing day.

The players begin by entering a tournament at the arena in Oberloch. They are recommended because they've already taken part in small bouts and proved themselves more than capable. This tournament will bring them prestige, access to privilaged information, treasure, and (more importantly) the attention of the League of the Iron Key, a resistance organisation undermining the Guild of Lochmen.

The party will be convinced to help the league break out some political prisoners from the hidden and secret high security prison run by guild agents, The Blackwater.

Unbeknownst to the party the League is backed by the Ascanan Protectorate hoping to destabalise the Guild. The prisoners to be exfiltrated are Protectorate agents who don't even know who the League is.

