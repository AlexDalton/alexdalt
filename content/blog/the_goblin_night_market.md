---
title: "The_goblin_night_market"
date: 2024-06-01
categories: [""]
draft: true
---

A coin appears in each party members pocket, an ancient tarnished golden piece

Come kiddies come grownups come from far and wide The goblin night market's in town Where dreams are sold and futures bought The goblin night market's in town Everything has a price, and a price that's so nice The goblin night market's in town In the depths of the earth Follow the symbol of worth For the goblin night market's in town

In the delve, if the players look well enough they can find the small wooden door in an alleyway.
The League of the Iron Key

    Lore dump about the shadowy figures in control of oberloch
    One of our brothers was taken (in reality they're being paid to free an Ascanan Protectorate super soldier captured while on leave in Oberloch)

