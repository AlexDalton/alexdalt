---
title: "Kew Gardens"
draft: false
archive: false
description: "A rainy day out in Feburary"
thumb: 'thumb.jpg'
date: 2024-02-22
---

{{< load-photoswipe >}}
{{< gallery dir="photography/2024_kew/" caption-position="none" />}}
