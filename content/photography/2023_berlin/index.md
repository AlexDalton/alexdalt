---
title: "Berlin"
draft: false
archive: false
description: "An anniversary city break"
thumb: 'thumb.jpg'
date: 2023-01-15
---

{{< load-photoswipe >}}
{{< gallery dir="photography/2023_berlin/" caption-position="none" />}}
