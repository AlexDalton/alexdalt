---
title: "York"
draft: false
archive: false
description: "An anniversary trip"
thumb: 'thumb.jpg'
date: 2024-01-19
---

{{< load-photoswipe >}}
{{< gallery dir="photography/2024_york/" caption-position="none" />}}
