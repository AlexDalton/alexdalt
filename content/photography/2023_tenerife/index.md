---
title: "Tenerife"
draft: false
archive: false
description: "A family holiday"
thumb: 'thumb.jpg'
date: 2023-07-11
---

{{< load-photoswipe >}}
{{< gallery dir="photography/2023_tenerife/" caption-position="none" />}}
