---
title: "One Month With Rory"
date: 2024-10-26
description: "Our lovely Gerberian Shepsky rescue pup"
archive: false
thumb: thumb.jpg
---

{{< load-photoswipe >}}
{{< gallery dir="photography/2024_rory/" caption-position="none" />}}
