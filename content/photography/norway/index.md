---
title: "Norway"
draft: false
archive: false
description: "An extended trip after EuroCrypt 2022"
thumb: 'thumb.jpg'
date: 2022-06-10
---

{{< load-photoswipe >}}
{{< gallery dir="photography/norway/" caption-position="none" />}}
