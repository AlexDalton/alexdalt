---
title: "Camping in Kent"
draft: false
archive: false
description: "A weekend spent an hour outside of London"
thumb: 'thumb.jpg'
date: 2024-07-28
---

{{< load-photoswipe >}}
{{< gallery dir="photography/2024_kent/" caption-position="none" />}}
