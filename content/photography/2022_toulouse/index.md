---
title: "Toulouse"
draft: false
archive: false
description: "A lovely trip, which involved England getting knocked out of the world cup"
thumb: 'thumb.jpg'
date: 2022-12-11
---

{{< load-photoswipe >}}
{{< gallery dir="photography/2022_toulouse/" caption-position="none" />}}
