---
title: "Graduation"
date: 2024-10-16
description: "Walking away with a PhD in Electrical and Electronic Engineering from Imperial College London"
archive: false
thumb: thumb.jpg
---

{{< load-photoswipe >}}
{{< gallery dir="photography/2024_graduation/" caption-position="none" />}}
