---
title: "Sam's Stag and Subsequent Wedding"
draft: false
archive: false
description: "A wholesome weekend in the Brecon Beacons and an even more wholesome follow-up weekend in Kent"
thumb: 'thumb.jpg'
date: 2024-06-29
---

The stag-do featured some truly freezing wild swimming and a quick trip up Pen y Fan.

{{< load-photoswipe >}}
{{< gallery dir="photography/2024_sams_wedding" caption-position="none" />}}
