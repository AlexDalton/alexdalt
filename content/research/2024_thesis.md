---
title: "Doctoral Thesis: Towards Secure Distributed Computation"
date: 2024-08-29
available: "/research-docs/Dalton-A-2024-PhD-Thesis.pdf"
authors: "A. Dalton"
archive: false
draft: true
---

Distributed computation is a computational paradigm in which a number of
discrete
computers are connected via a network and collaborate on the execution of
programs. These
systems exist in a variety of contexts: volunteer scientific computation,
blockchain, and file sharing swarms for example. Volunteer scientific computing
is most closely related to this work, and
these schemes assume altruistic behaviour from their participants
as nodes are volunteers
donating their computational power for the universal good.
However, a number of open questions exist:
Can network protocols be orchestrated
in such a way as to encourage altruistic behaviour from nodes that would otherwise
be unmotivated to collaborate? How could a decentralised computation network be hardened
against adversarial actors interested in disrupting computations? Are there any
incentives that need to be put into place to encourage altruistic behaviour in
otherwise self-motivated network participants?

The main identified vulnerabilities in a distributed
computation
protocol identified in this work are:
* Correctness of delegated program results.
* Discouraging self-motivated behaviours.
* Confidentiality of private data.

This work provides a class of distributed computation protocol that supports all three:
a TCM, the properties of which are defined
in this thesis. The exemplar TCM developed as part of this work is
DSCS. DSCS was
specifically designed to support resource-constrained nodes, lowering the barrier of entry
to a variety of distributed devices.
The objective of this work is to create a secure distributed computation
protocol, open to anonymous nodes with access to varying computational
power. 

This thesis aims to provide a specification for a secure-by-default decentralised
distributed computation scheme, in which non-homogeneous untrusted nodes can
organically distribute work. There are a variety of contributions; new cryptographic
constructions and protocol formulations, as well as a reference protocol implementation.
Most of the contributions are purely theoretical in nature, with some claims
supported by implementation work later on.

Proofless Verifiable Computation from Residue Number Systems is a new
VC scheme. This work provides a protocol in which a Verifier outsources a
computation to a Prover, and it is computationally intractable for the Prover to return an incorrect
result that will be accepted by the Verifier. The scheme has a small constant time setup, no program
preprocessing, and is always Outsourceable. The Verifier
requires no proof of correctness from
the Prover. It is Prover-optimal in the Prover's computational complexity, and
the total time taken from initialising a problem through to receiving and verifying
a result only takes a single modular reduction and integer comparison longer than executing the program
via a normal unverified remote computation protocol.

Two Party Fair Exchange provides a FE
protocol without the involvement of a trusted third party, a result previously thought to be impossible.
To the best of my knowledge the FE protocol presented in
this work is the first to recognise it is not impossible to support fair exchange
between only two parties, and is the first concrete protocol to do so.

DSCS is a protocol for supporting secure-by-default decentralised distributed
computation. It is an example of a new class of construction, a TCM, to
which relevant definitions and proofs are provided.
 DSCS makes black box use of a number
 of cryptographic building blocks including VC, FE, and LFHE.

DiSCuS is a partial DSCS implementation used to generate
real-world performance measurements and verify claims about the capabilities
of DSCS. It includes algorithmic details not required for the DSCS
definition but that are required for a working implementation, informed the
development of DSCS, and clearly signposts future work.

The work in this thesis draws on new developments in cryptography to build an egalitarian
decentralised distributed computation network. A number of supporting technologies
are created or, where solutions already exist, improved upon. These
technologies are later incorporated into the distributed computation network
protocol DSCS and the corresponding implementation DiSCuS.
