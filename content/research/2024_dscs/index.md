---
title: "DSCS: Towards an Extensible Secure Decentralised Distributed Computation Protocol"
date: 2024-09-24
venue: IEEE CSR 2024
available: "https://ieeexplore.ieee.org/document/10679478"
authors: "A. Dalton, D. Thomas, and P. Cheung"
archive: false
---

This paper documents DSCS, a privacy preserving distributed computation protocol with native support for homomorphic encryption. The protocol is easily extensible with advanced cryptographic constructions due to a program encoding with a bijective map to arithmetic circuits. Arithmetic circuits are a common mechanism for abstract computation, and are widely used by advanced cryptographic constructions, such as homomorphic encryption and verifiable computation. This paper documents foundational work exploring the validity of an arithmetic circuit based approach to distributed computation. Ultimately, the direct use of arithmetic circuits as a computational substrate for distributed computation is shown to carry an untenable bandwidth cost, even under ideal network conditions. Importantly, this paper highlights a slew of low hanging fruit for further research in this domain, as well as guidance to avoid easy pitfalls.

{{< pdf-embed src="./paper.pdf" >}}
