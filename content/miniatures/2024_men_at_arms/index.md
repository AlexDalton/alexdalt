---
title: "Warhost of the Once-Duke: Chapter 1"
draft: false
archive: false
description: "The start of some Bretonnians for The Old World"
collections: ['Warhost of the Once-Duke', 'The Old World']
thumb: 'thumb.jpg'
date: 2024-03-27
---

# Chapter 1

An uncharacteristic fog enveloped these hills three weeks ago. Odd, for this
time of year. For three weeks
folk navigated by sound, for sight was only good for an advanced warning of fresh
corpses. The fresh corpses were similarly odd for this time of year.

At the end of the third week the fog receded, as suddenly as it has arrived.
Locals noted that things were much as
they were before the fog descended. One by one, however, they came to notice the
fort silhouetted on
a distant hill. Where once there was little other than grazing sheep now sat a
haphazard coalition of spires, towers, and battlements. The structures
arranged without sense and in, what first appeared to be, an advanced state of
decay. This monolithic impression on the public conciousness began to draw madness and recklessness from
those who looked upon it, and in turn the mad and reckless were drawn to it.

First to investigate were the foolhardy sons and daughters of those nearby. As
they
indulged their curiosity they discovered that despite first appearances this
was not a ruin, but a maddened construction site in some sort of superposition of assembly
and disassembly. Ragged men and women swarmed
over structures, repurposing materials for new buildings, which were almost as
quickly dismantled and repurposed themselves. Despite the expenditure of effort,
it seemed as if an equilibrium exists - the fort
maintaining a constant size, organically shifting under the frantic
guidance of the workmen.

It was clear to those performing this early reconnaissance that
as strange as The Shifting Keep, as it became known, was, the men and
women who came in it's wake were stranger still. A codependent ecosystem
of the destitute,
the mad, and the strong. Knights demanded the construction of tourney grounds
and feasting halls. Wizards demanded the construction of taller and
stranger towers. Clergymen demanded ever more ornate cathedrals. The Toilmen
shaped, hauled, and reshaped stone as was demanded of them.

These strangers enforced their will on the surrounds. Impregnable behind their
ever shifting citadel.

{{< figure src="DSCF3605-Edit.jpg" title="Freemen of the Once-Duke" >}}

Stripped of his title and lands the Once-Duke Eadifir Crepuscule would be dammed
if he’d let them take his castle too. A well timed amendment to local law
meant that the peasantry were not bound to the
land, but were in fact bound to the ducal seat of power - his castle. Thus, where ever
he took his castle those that served him would come to. 
Roaming the world with a baggage train of artisans and servile labour his castle is transported
brick by brick. His knights followed suit, of course. Bound to his will as they
are by a dense
web of oaths, gifts, and threats, his strength comes from their willingness to
subjugate on his behalf. Through them resources are extracted until the time comes to move onto
pastures new.

The Once-Duke is not blind to his vulnerability however. There is a critical
stage in the deconstruction, transport, and reconstruction of his holdings where
he could be subject to attack with little in the way of defensible fortifications.
Thankfully, a mobile
lifestyle of noble luxury suits wizards of less legitimate training, and his court welcomes
them.
Court is rife with mages, soothsayers, and his favoured weather wizards, who in
return for impunity, distort the fabric of the reality to obfuscate the arrival
and subsequent departure of The Shifting Keep.

{{< figure src="DSCF3649-Edit.jpg" title="Jon the Unanointed" >}}

Months passed. Attempts to repel The Shifting Keep were dashed against the ever
changing battlements. This region was bled dry. Nothing but starving goatherds
and empty barns. Those that did not join the ranks of the Warhost of the
Once-Duke will die to starvation, disease, and infighting.

The fog descends. It is time to move on to pastures new.
