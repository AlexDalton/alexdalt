---
title: "The Mallard Men of Mudby Marsh"
draft: false
archive: false
description: "A steamer for the game Scabz"
collections: ['Turnip28', 'Scabz', 'Salute Fringe']
thumb: 'DSCF9193-Edit.jpg'
date: 2024-11-22
---

It's a rubber duck with a baked bean tin mashed into it. The baked bean tin
rotates to indicate which side of the craft the broadsides are facing. The crew
have ducks for heads - think Ratatouille if it was a horror film about ducks.

{{< figure src="./DSCF9103-Edit.jpg" title="A mother and her ducklings (they're torpedoes)" >}}
{{< figure src="./DSCF9152-Edit.jpg" title="The crew are a solemn bunch, they don't say much." >}}
{{< figure src="./DSCF9193-Edit.jpg" title="It's bath time motherf*cker." >}}

The creation of this monstrosity was targeting a Scabz event being run by
[Salute Fringe](https://www.instagram.com/salutefringe/). It was a gruesome
affair, as desperate rival steamers blasted and shunted their way around the
tables of Bad Moon Cafe.

{{< figure src="./DSCF9215.jpg" title="This one had been boiled in sugar to encourage the growth of crystals." >}}
{{< figure src="./DSCF9217.jpg" title="This one is made of paper!" >}}
{{< figure src="./DSCF9223.jpg" title="This one has a periscope made from the head of an electric toothbrush." >}}
{{< figure src="./DSCF9225.jpg" title="This one reminds me of an elderly relative." >}}
{{< figure src="./DSCF9249.jpg" title="I'm not sure what this guy is doing here, but I'd feel the same in his position." >}}

A lovely event, with lovely folk in attendance! It will always be Eel Day in my heart.
