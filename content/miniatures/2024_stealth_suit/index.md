---
title: "A Solitary Stealth Suit"
draft: false
archive: false
description: "Lonely, resplendent in it's isolation"
collections: ['The Brightcharge Vanguard', 'Warhammer 40k']
thumb: 'DSCF9026-Edit.jpg'
date: 2024-09-18
---

Try-harded a bit too close to the sun on this one, the chance of the whole squad ever being finished is near zero.

{{< figure src="./DSCF9026-Edit.jpg">}}
{{< figure src="./DSCF9049-Edit.jpg">}}
{{< figure src="./DSCF9066.jpg">}}

