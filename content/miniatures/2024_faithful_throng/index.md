---
title: "The Faithful Throng"
draft: false
archive: false
description: "The newest recruits of The Cult of the Pallid Emperor"
collections: ['Cult of the Pallid Emperor', 'Warhammer 40k']
thumb: 'DSCF9027.jpg'
date: 2024-07-29
---

Filling out the ranks of The Cult of the Pallid Emperor.

Another 20 odd cultists for the hoard. In GSC you can never have enough Neophyte
Cultists. A large part of this blob are enough special weapons to satisfy the
Neophytes new box-locked status. Before you could take duplicate heavy weapons and
duplicate special weapons, now you can take two different heavy weapons and two 
different special weapons.

{{< figure src="./DSCF9029-Edit.jpg" title="The newest members of The Faithful Throng" >}}

From the one game I've played since being box-locked, rolling attacks for so many
largely inconsequential special weapons is a massive pain during gameplay. If I
wanted to play Kill Team, I would!

With Acolyte Hybrids split into two different datasheets depending on if you want
flamers (and demo charges) or autopistols (and mining tools) I needed three new
models to fill out a legal unit.

{{< figure src="./DSCF9004-Edit.jpg" title="Three members of The Brotherhood of the Pale Claw" >}}

I converted some Genestealers using some Nighthaunt bits, with cloaks made from
blue roll and PVA glue. The initial paint job was mostly a black prime, wit layered
red and yellow undershading. However, during use on the tabletop (when seen from a 45 degree angle)
they looked like black unpainted minis. I pushed the contrast on the bases, trying
to invoke smoldering molten wreckage, and this allowed the mostly black minis to
be silhouetted against the bright bases on the tabletop.

{{< figure src="./DSCF9015-Edit.jpg" title="A swarm of The Gene Kissed" >}}

Although I'm not entirely happy about the deviation from the basing scheme, the
improved tabletop readability is a boon.
