---
title: "Some Cultists"
draft: false
archive: false
description: "Filling out the special weapon options for my Neophyte Hybrids"
collections: ['Cult of the Pallid Emperor', 'Warhammer 40k']
thumb: 'DSCF4474.jpg'
date: 2024-06-02
---

Also an experimentation with more dramatic lighting and handheld miniature
photography without focus stacking. Kitbashed, painted, and based in a couple of
days ahead of a game of 40k Combat Patrol.

{{< figure src="./DSCF4510.jpg" title="Three best friends" >}}
{{< figure src="./DSCF4466.jpg" title="Little John" >}}
{{< figure src="./DSCF4474.jpg" title="Jeremy is very ill..." >}}
{{< figure src="./DSCF4478.jpg" title="Terrance" >}}
