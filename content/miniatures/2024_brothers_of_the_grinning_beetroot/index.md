---
title: "TurnipCon V & The Brothers of the Grinning Beetroot"
draft: false
archive: false
description: "The Uprising of the Louse for TurnipCon V"
collections: ['Turnip28']
thumb: 'DSCF9151.jpg'
date: 2024-11-15
---

After many months of tinkering, procrastinating, and productivity I finished my
Turnip28 army mere hours ahead of TurnipCon V. I present, The
Brothers of the Grinning Beetroot.

The unwashed masses flock to the titanic toothy root to be embraced and forever
changed as they are hoisted by the neck into it's tangle of grasping root matter.
Their pains of forgiven, their previous indiscretions forgotten, and they march
forward towards unbelievers.

{{< figure src="./DSCF9118-Edit.jpg" title="The Brothers of the Grinning Beetroot" >}}
{{< figure src="./DSCF9133.jpg" title="Fodder and The Voice of the Root" >}}
{{< figure src="./DSCF9136.jpg" title="Some Rabble" >}}
{{< figure src="./DSCF9158-Edit.jpg" title="The Grinning Beetroot" >}}
{{< figure src="./DSCF9143.jpg" title="In the shadow of the beetroot..." >}}
{{< figure src="./DSCF9145.jpg" title="...the unwashed masses queue for their execution" >}}
{{< figure src="./DSCF9152.jpg" title="Jason (and Other Jason) supervise the Rabble" >}}
{{< figure src="./DSCF9209-Edit.jpg" title="'Land Ho' says The Voice of the Root" >}}
{{< figure src="./DSCF9218-Edit.jpg" title="The men carrying his craft shuffle forward" >}}

My first TurnipCon was a lovely affair, and well worth the 6am train from London
Paddington. Everyone was friendly, enthusiastic, and the sheer hobby talent was
nothing if not decadent! Every game-in-progress looked like a diorama set up to
reflect some nightmare scene of peasants kicking each other to death in the mud
at the behest of incompetent toffs. The twist for this TurnipCon, it was a
doubles event - with the pairings for each game randomly allocated.

{{< figure src="./DSCF9002.jpg" title="Bristol Independent Gaming, the perfect venue!" >}}

My first game was against the inspirational
[@apocripha_now](https://www.instagram.com/apocrypha_now/) alongside the
indomitable [@plastic_cracked](https://www.instagram.com/plastic_cracked/). What
we thought was going to be a landslide victory was snatched from us at the last
moment by some cunning plays! Good game sirs!

{{< figure src="./DSCF9005.jpg" title="I don't know what this is but I'm glad it's on my team." >}}
{{< figure src="./DSCF9017.jpg" title="No, I don't think I will!" >}}
{{< figure src="./DSCF9027.jpg" title="My peasants, alone, facing down a sea of rootlings." >}}
{{< figure src="./DSCF9034.jpg" title="An allied Root Shrine Toff-Offs." >}}
{{< figure src="./DSCF9036.jpg" title="Good god I'm glad this is on my team." >}}
{{< figure src="./DSCF9045.jpg" title="The aforementioned monstrosity was killed by this little freak." >}}

Lunch was a perfectly serviceable affair. I ran into Ben, of 
[Games Night](https://www.youtube.com/@GamesNight) fame, who was taking part
in a Saga tournament next door, blissfully unaware of the muddy freak show
happening beyond. We chatted a bit, he seems perfectly nice.

My second game was as fun as the first - and just as
disastrous for my tournament score!

{{< figure src="./DSCF9058.jpg" title="The enemy! The swines!" >}}
{{< figure src="./DSCF9074.jpg" title="The swine!" >}}
{{< figure src="./DSCF9061.jpg" title="Dredged up from what's left of the Seine." >}}
{{< figure src="./DSCF9069.jpg" title="A rare and beautiful beastie." >}}

The third game was a rare occurrence in the tournament - a win for me! Probably
aided in part by the fact that one of the opponents was actively trying
to lose in order to complete a bingo card...

{{< figure src="./DSCF9079.jpg" title="He looks sad." >}}
{{< figure src="./DSCF9082.jpg" title="As do these." >}}
{{< figure src="./DSCF9083.jpg" title="Crabs with guns!" >}}
{{< figure src="./DSCF9093.jpg" title="Brace for a charge!" >}}
