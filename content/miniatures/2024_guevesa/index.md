---
title: "Gue'vesa Breachers"
draft: false
archive: false
description: "Desperate humans fighting for the T'au"
collections: ['The Brightcharge Vanguard', 'Warhammer 40k']
thumb: 'DSCF9095-Edit.jpg'
date: 2024-09-11
---

Those humans who, out of desperation, turn to the T’au often find themselves thrust to the front lines, sent to kill their kin for The Greater Good.


{{< figure src="./DSCF9095-Edit.jpg" title="Gue'vesa Breachers" >}}
