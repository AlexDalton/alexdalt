---
title: "The Brightcharge Vanguard"
draft: false
archive: false
description: "Towards 1000 points of T'au for Warhammer 40,000"
collections: ['The Brightcharge Vanguard', 'Warhammer 40k']
thumb: 'thumb.jpg'
date: 2023-12-28
---

Supply lines slashed, and resources running low, the T'au of The Brightcharge
Vanguard survive on scavenged parts, hasty field repairs, and a healthy dose of
pragmatism.

{{< load-photoswipe >}}
{{< gallery dir="minis/2023_brightcharge" caption-position="none" />}}
