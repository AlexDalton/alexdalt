---
title: "Cult of the Pallid Emperor"
draft: false
archive: false
description: "Genestealer Cult Combat Patrol for Warhammer 40,000"
thumb: 'thumb.jpg'
collections: ['Cult of the Pallid Emperor', 'Warhammer 40k']
date: 2023-12-27
---

Worshipping eldritch abominations from beyond the galactic rim, these desperate
souls will rise up during the coming flood.

{{< load-photoswipe >}}
{{< gallery dir="minis/2023_cult" caption-position="none" />}}
