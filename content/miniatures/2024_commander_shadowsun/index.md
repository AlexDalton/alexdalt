---
title: "Commander Shadowsun"
draft: false
archive: false
description: "A T'au named character"
collections: ['The Brightcharge Vanguard', 'Warhammer 40k']
thumb: 'thumb.jpg'
date: 2024-03-26
---

{{< figure src="./DSCF3567-Edit.jpg" >}}
{{< figure src="./DSCF3567-Edit-2.jpg" >}}
