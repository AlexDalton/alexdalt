---
title: "Two Little Red Pandas"
draft: false
archive: false
description: "A christmas present for my partner"
thumb: 'thumb.jpg'
date: 2024-01-25
collections: ['Misc Miniatures']
---

{{< figure src="./DSCF3284-Edit.jpg" title="This One" >}}
{{< figure src="./DSCF3300-Edit.jpg" title="That One" >}}
{{< figure src="./DSCF3345-Edit.jpg" title="Both" >}}
