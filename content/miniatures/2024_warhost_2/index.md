---
title: "Warhost of the Once-Duke: Chapter 2"
draft: false
archive: false
description: "1,000pts of Bretonnians (almost)"
collections: ['Warhost of the Once-Duke', 'The Old World']
thumb: 'thumb.jpg'
date: 2024-07-10
---

This is the (almost complete) second phase of Bretonnians I've painted for an
Old World narrative slow grow campaign being run by the London Wargaming Guild.
I still owe them four Knights of the Realm and a painted Baron on Hippogryph, but that's
going back into my personal mini painting backlog for now...

[Chapter 1 is given here.](/tabletop/2024_men_at_arms)

---

# Chapter 2

The campaign across the Border Princes has been a straight forward affair, with frequent
losses relegated to the ranks of the peasantry, vacancies which are easily replaced
by the unwashed hoards flocking to walls of the Shifting Keep. The
insatiable ogres and scions of the empire took segments of the low-born as
tribute, but the peasants are nothing if not disposable.
The Warhost swelled as the weak minded felt the pull of The Shifting Keep. An
insidious current, drawing in those impressionable enough to be press ganged into
service.

Gurt was one such unfortunate. Feeling the draw of The Shifting Keep he found
himself wandering the grounds immediately beyond the walls. Brutes grabbed Gurt, placed a bow in his
hands, and put him before an Ogre charge. Understandably he ran, and
understandably he was punished.

{{< figure src="./DSCF4760.jpg" title="Gurt the Fleer" >}}

Gurt became a spotter. Lucky to avoid more esoteric punishments at the
hands of the Once-Duke's wizardly court, he was allowed to remained Gurt in
mind, and mostly in body. His legs were removed so he could not embarrass himself
on
the field of battle again. He was bound to a post and placed above the defensive
ramparts of the archers. He would use his height advantage to call targets to his
more steadfast peers.

{{< figure src="./DSCF4716-Edit.jpg" title="Toilmen" >}}

The court of the Once-Duke continued to grow, and festered a cohort of
self-taught magisters and errant spellcrafters.
One weather wizard in particular is a fixture at The Once-Duke's side. Windrip, a
crooked elementalist, has long since provided his expertise in meteorological
strategy to the Once-Duke in exchange for table scraps and test subjects.

{{< figure src="./DSCF4680-Edit.jpg" title="Windrip, the wizard, and his homunculus Smeeps" >}}

Test subjects are required as Windrip
has a certain proclivity for homunculi and their devilish antics. He finds their childlike
wonder and propensity for mischief much improved over the grovelling impoverished
stock they are sculpted from.

The Honourable Traitors to the Realm are a band of knights sworn to the Once-Duke
by such an oppressive web of oaths and allegiances that they were honour-bound to
break their vows to the realm and go into exile with the Once-Duke. The first of
these oxymoronic knights is Pyrrole, the Ratmaster.

{{< figure src="./DSCF4786-Edit.jpg" title="Pyrrole, the Ratmaster" >}}

Pyrrole holds lowly court with spies and informants and
filters this information for the ears of those who keep cleaner
company. The knight that holds the position of Ratmaster forfeits his knighthood
in order to associate with the lesser men, and as such straddles two worlds yet is accepted in
none. His position as the first knight of
the Honourable Traitors to the
Realm was awarded due to subtle subservience to the Once-Duke and
clever politicking, much
to the chagrin of the rest of the knightly order.

Sir Durgur Blackvain is one such dissenting knight, a member of the old guard, who followed
the Once-Duke into the borderlands because of ancient and esoteric oaths. He has
forfeited much to maintain the oath sworn generations past, which has gone largely
unrecognised by the Once-Duke.

{{< figure src="./DSCF4815-Edit.jpg" title="Sir Durfur Blackvain" >}}

Sir Robert is an oddity amongst the knights, a halfling. This diminutive warrior was permitted
to join the ranks of the Honourable Traitors as a jest, perpetrated by the
Once-Duke, to slight the grumbling knights of the order. Sir Robert is aware of
his position as both vector for punishment and jester, yet he does not allow it
to influence his virtue on the battlefield. He knows his story will be defined
by his deeds, made greater and not worse by his humble origins.

{{< figure src="./DSCF4400.jpg" title="Sir Robert, The Carrot Knight" >}}

{{< figure src="./DSCF4450.jpg" title="Lady Bethnee of Reeve" >}}

The second order of knights under the command of the Once-Duke is the that of The
Triptych Knights. These self-indulgent knights worship at the alter of excess,
for it is the only thing that separates the nobility from the peasantry. Feasts,
blood sport, intoxicants and other pleasures of the flesh are signs of true
virtue to these knights.

{{< figure src="./DSCF4841-Edit.jpg" title="Sir Thomas the Thrice Dead" >}}

First of the Triptych Knights is Sir Thomas.
On three separate occasions Sir Thomas has awoken amidst a pile of bodies in a battle
long since gone cold. Presumed dead, pockets emptied by opportunistic survivors,
bones often fractured,
he has dragged himself back to the Shifting Keep. He considers his oaths to the
Once-Duke so binding that his soul will not leave his body without explicit
permission. He is a living reminder to gut the corpse who's pockets you are picking.

{{< figure src="./DSCF4825-Edit.jpg" title="Lady Bryne, Theiftaker" >}}

Lady Bryne is tasked with the capture and corresponding execution of any peasantry
who would steal objects belong to their betters. This is a duty
she carries out swifty, brutally, and with complete impunity. The items belonging
to the duly charged are claimed by the victim of the crime, with a sizeable bounty
conferred to Lady Bryne herself.
