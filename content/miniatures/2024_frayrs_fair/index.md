---
title: "Freyr's Fair"
draft: false
archive: false
description: "A Norse Blood Bowl Team"
collections: ['Misc Miniatures', 'Blood Bowl']
thumb: 'thumb.jpg'
date: 2024-04-16
---

A hardy team of drunkards, wolfmen, and little piggies. Ready for Blood Bowl.

{{< figure src="DSCF3909.jpg" title="Frayr's Fair" >}}

