---
title: "Brood Brothers"
draft: false
archive: false
description: "The men and women of the planetary defence force are sympathetic to our cause"
collections: ['Cult of the Pallid Emperor', 'Warhammer 40k']
thumb: 'thumb.jpg'
date: 2024-03-26
---

{{< figure src="thumb.jpg" title="Brood Brothers" >}}
