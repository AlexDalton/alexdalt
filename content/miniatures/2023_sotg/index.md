---
title: "The Sons of the Gorgon"
draft: false
archive: false
description: "A handful of Space Marines for Warhammer 40,000"
collections: ['Misc Miniatures', 'Warhammer 40k']
thumb: 'thumb.jpg'
date: 2023-12-26
---

Operating as a distributed network of independent warbands, the command structure
of The Sons of the Gorgon is an intricate latticework of cause and effect.

{{< load-photoswipe >}}
{{< gallery dir="minis/2023_sotg" caption-position="none" />}}
