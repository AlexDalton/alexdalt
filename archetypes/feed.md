---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
aliases:
    - "/{{ substr (.Name | md5) 0 8 }}"
type_of_post: note
syndecated:
---

