---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
aliases:
    - "/{{ substr (.Name | md5) 0 8 }}"
resources:
    - src: "**.jpg"
      name: img-:counter
type_of_post: photos
syndecated:
---

